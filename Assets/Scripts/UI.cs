﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
	public GameObject pausePanel;
	public void Pause()
	{
		Time.timeScale = 0;
		pausePanel.SetActive(true);
	}
	public void LoadLevel(int level)
	{
		SceneManager.LoadScene(level);
	}
	public  void Continue()
	{
		Time.timeScale = 1;
		pausePanel.SetActive(false);
	}
	public void Home()
	{
		//download main menu
	}
	public void Exit()
	{
		Application.Quit();
	}
}
