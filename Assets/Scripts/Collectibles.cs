﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collectibles : MonoBehaviour
{
	public Text potion, heath, money;
	public int potionCount, moneyCount = 0;
	public int heathCount = 3;
	private void Start()
	{
		potion.text = potionCount.ToString();
		heath.text = heathCount.ToString();
		money.text = moneyCount.ToString();
	}
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.gameObject.tag == "Potion")
		{
			potionCount++;
			potion.text = potionCount.ToString();//will add "Press E butt(on)"
			collision.gameObject.SetActive(false);
		}
		else if(collision.gameObject.tag == "Health" && heathCount != 3)
		{
			heathCount++;
			heath.text = heathCount++.ToString();
			collision.gameObject.SetActive(false);
		}
		else if (collision.gameObject.tag == "Money")
		{
			moneyCount++;
			money.text = moneyCount++.ToString();
			collision.gameObject.SetActive(false);
		}
	}
}
