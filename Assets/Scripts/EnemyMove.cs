﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    private bool isAlive = true;
    public Transform playerTransform;
    public GameObject player;
     HeroKnight playerScript;
    public BoxCollider2D BoxCollider2D;
    public Rigidbody2D rb;
    //variables
    public int moveSpeed;
    public int health = 3;
    private bool canAttack = true;
    //movement
    public float followRadius;
    //end
    [SerializeField] Animator enemyAnim;
    SpriteRenderer enemySR;
	private void OnTriggerStay2D(Collider2D collision)
	{
        if (collision.gameObject.tag == "Player")
        {
            canAttack = false;
            enemyAnim.SetInteger("AnimState", 0);
            enemyAnim.SetBool("Attack", true);
        }
    }
	private void OnTriggerExit2D(Collider2D collision)
	{
        canAttack = true;
        enemyAnim.SetBool("Attack", false);
    }

    void Start()
    {
        //enemy animation and sprite renderer 
        enemyAnim = gameObject.GetComponent<Animator>();
        enemySR = GetComponent<SpriteRenderer>();
        playerScript = player.GetComponent<HeroKnight>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            if (checkFollowRadius(playerTransform.position.x, transform.position.x))
            {
                if (playerTransform.position.x < transform.position.x && canAttack)
                {
                    transform.position += Vector3.left * moveSpeed * Time.deltaTime;
                    enemyAnim.SetInteger("AnimState", 2);
                    enemySR.flipX = false;
                }
                //if player is behind enemies
                else if (playerTransform.position.x > transform.position.x && canAttack)
                {
                    transform.position += Vector3.right * moveSpeed * Time.deltaTime;
                    enemyAnim.SetInteger("AnimState", 2);
                    enemySR.flipX = true;
                }
            }
        }
    }
    public void TakeDamage()
	{
        health--;
        enemyAnim.SetTrigger("Hurt");
        if (health == 0)
		{
            enemyAnim.SetBool("Death", true);
            enabled = false;
            Destroy(rb);
            BoxCollider2D.enabled = false;
        }
	}
    public void GiveDamage()
	{
        playerScript.TakeDamage();
        if(player.GetComponent<Collectibles>().heathCount == 0)
		{
            isAlive = false;
            enemyAnim.SetTrigger("EnemyDead");
        }
    }
    public bool checkFollowRadius(float playerPosition, float enemyPosition)
    {
        if (Mathf.Abs(playerPosition - enemyPosition) < followRadius)
        {
            //player in range
            return true;
        }
        else
        {
            return false;
        }
    }
}
