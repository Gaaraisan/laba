﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
	public int level;
	private void OnTriggerEnter2D(Collider2D collision)
	{
		SceneManager.LoadScene(level);
	}
	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.E))
		{
			SceneManager.LoadScene(Application.loadedLevel);
		}
	}
}
