﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : Interactable
{
	protected float weight;
	protected override void Interact()
	{
		//піднімає предмет
	}
}
public class HealPotion : Item
{
	private int healPoints;
	protected virtual void Use()
	{
		//відновлює здоров'я гравцю
	}
}
public class FirePotion : HealPotion
{
	private int damage;
	protected override void Use()
	{
		//підвищує силу гравця
	}
	private void Throw()
	{
		//гравець кидає зілля у ворога
	}
}
public class Money : Item
{
	private int value;
	private void Throw()
	{
		//кидає монетку, щоб відволікти ворога
	}	private void Bless()
	{
		//віднімає
	}
}
