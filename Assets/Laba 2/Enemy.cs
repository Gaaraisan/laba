﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Interactable
{
	protected int damage;
	protected int health;
	protected virtual void Attack()
	{
		//атакує гравця
	}
}
public class MleeEnemy : Enemy
{
	protected virtual void Bomb()
	{
		//кидає фізичну бомбу в гравця
	}
	private void Shield()
	{
		//блокує атаки щитом
	}
	private void Dash()
	{
		//ухиляється від ударів гравця 
	}
}
public class MagicEnemy : MleeEnemy
{
	private int inviseTime;
	private int magicAttack;
	protected override void Bomb()
	{
		//кидає магічий заряд в гравця
	}
	private void Invisible()
	{
		//стає невидимим, щоб не вмерти від гравця
	}
}
