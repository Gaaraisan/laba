﻿using System.Collections.Generic;
using UnityEngine;

public abstract class NPC : Interactable
{
	private int numberOfQuests;
	private GameObject[] rewardsForQuest;
	private string[] repllicas;

	protected override void Interact()
	{
		//запускає діалог з гравцем
	}
	protected void GiveQuest()
	{
		//дає квест гравцю
	}
	protected void GiveReward()
	{
		//дає винагороду
	}
}
public class Human : NPC
{
	private int health;
	private int damage;
	private void Simulate()
	{
		//зображає діяльність людей
	}
	private void Attack()
	{
		//аттакує гравця, якщо той почав суперечку
	}
}
public abstract class Seller : NPC
{
	protected int money; 
	protected int playerRespect;
	protected virtual void Sell()
	{
		//продає гравцю предмет
	}
}
public class WeaponSeller : Seller
{
	private GameObject[] Weapons;
	private GameObject[] Armor;
	private int timeToMakeArmor;
	protected override void Sell()
	{
		//продає зброю
	}
	private void MakeArmor()
	{
		//виковує броню
	}
}
public class MagicSeller : Seller
{
	private List<GameObject> spells;
	private int costOfUpgrade;
	protected override void Sell()
	{
		//продає заклинання
	}
	private void TeachMagic()
	{
		//вчить магії 
	}
}
