﻿using UnityEngine;

public class Interactable : MonoBehaviour
{
	public bool isActive;
	protected virtual void Interact()
	{
		//Шаблон функції для взаємодії з гравцем
	}
}